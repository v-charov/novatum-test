export default (number: number): string => {
  return (+number.toFixed(2)).toLocaleString("en-US");
};
