import { defineStore } from "pinia";
import dataAccounts from "@/data/accounts.json";
import { IAccounts } from "@/store/accounts/interfaces";
import { ECurrencySymbol } from "@/store/currency/enums";
import numberFormat from "@/helpers/numberFormat";

export const useStoreAccounts = defineStore("storeAccounts", {
  state: () => ({
    dataAccounts,
  }),
  getters: {
    accounts(): IAccounts[] {
      return this.dataAccounts.data.map(
        (account): IAccounts => ({
          value: account.id,
          curdNumber: account.acc_no.slice(-4),
          name: account.acc_name,
          currency: {
            val: account.currency,
            symbol: ECurrencySymbol[account.currency],
          },
          balance: numberFormat(account.balance),
        })
      );
    },
  },
});
