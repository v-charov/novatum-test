export interface IAccounts {
  readonly value: string;
  readonly curdNumber: string;
  readonly name: string;
  readonly currency: {
    readonly val: string;
    readonly symbol: string;
  };
  readonly balance: string | number;
}
