import { defineStore } from "pinia";
import dataCurrency from "@/data/currency.json";
import { ICurrency } from "@/store/currency/interfaces";

export const useStoreCurrency = defineStore("storeCurrency", {
  state: () => ({
    dataCurrency,
  }),
  getters: {
    currency(): ICurrency[] {
      return this.dataCurrency.data;
    },
  },
});
