export interface ICurrency {
  readonly abbr: string;
  readonly amount: number;
  readonly value: number;
  readonly crypto: boolean;
}
