interface CurrencySymbolKeys {
  [key: string]: string;
}

export const ECurrencySymbol = {
  GBP: "£",
  USD: "$",
  EUR: "€",
  CNY: "¥",
} as CurrencySymbolKeys;
