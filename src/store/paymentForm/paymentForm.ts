import { defineStore } from "pinia";
import { useStoreAccounts } from "@/store/accounts/accounts";
import { useStoreCurrency } from "@/store/currency/currency";
import { IAccounts } from "@/store/accounts/interfaces";
import numberFormat from "@/helpers/numberFormat";

export const usePaymentForm = defineStore("paymentForm", {
  state: () => {
    const { accounts } = useStoreAccounts();
    const { currency } = useStoreCurrency();

    return {
      accounts,
      currency,

      selectValueFrom: null,
      selectValueTo: null,

      amount: null,
    };
  },
  getters: {
    optionForFrom(store): IAccounts[] {
      if (store.selectValueTo)
        return store.accounts.filter(
          (acc) => acc.value !== store.selectValueTo
        );

      return store.accounts;
    },
    optionForTo(store): IAccounts[] {
      if (store.selectValueFrom)
        return store.accounts.filter(
          (acc) => acc.value !== store.selectValueFrom
        );

      return store.accounts;
    },
    selectAccountFrom(store): IAccounts | undefined {
      return store.accounts.find((acc) => acc.value === store.selectValueFrom);
    },
    selectAccountTo(store): IAccounts | undefined {
      return store.accounts.find((acc) => acc.value === store.selectValueTo);
    },
    isSelectedCards(store): boolean {
      return ![store.selectValueFrom, store.selectValueTo].every((i) => i);
    },
    accountCurrencyFrom(): string | undefined {
      return this.selectAccountFrom?.currency.symbol;
    },
    accountCurrencyTo(): string | undefined {
      return this.selectAccountTo?.currency.symbol;
    },
    currencyRatio(store): string {
      let ratioValue: number | string = 1;
      const ratioFrom: number | undefined = store.currency.find(
        (c) => c.abbr === this.selectAccountFrom?.currency.val
      )?.value;
      const ratioTo: number | undefined = store.currency.find(
        (c) => c.abbr === this.selectAccountTo?.currency.val
      )?.value;

      if (ratioFrom && ratioTo && ratioFrom !== ratioTo) {
        ratioValue = numberFormat(ratioFrom / ratioTo);
      }

      return this.accountCurrencyFrom && this.accountCurrencyTo
        ? `${this.accountCurrencyFrom} 1 = ${this.accountCurrencyTo} ${ratioValue}`
        : " ";
    },
  },
});
